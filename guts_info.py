#!/usr/bin/python3

import argparse
import re
import math

argParser = argparse.ArgumentParser(description="Manage way of displaying values with:")
argParser.add_argument('--block-size', type = str, default = "kB",help = "Set block size. Default is kB.")
args = argParser.parse_args()

def block_size_div(block_id):
    return {
        "kB":   1,
        "k":    1,
        "MB":   1024,
        "M":    1024,
        "GB":   1024*1024,
        "G":    1024*1024,
    }.get(block_id, 1)

# Initialization of used values
CPU_count   = 0
CPU_model   = ""
memory      = ""
CPU_cache   = ""
OS_release  = ""

with open("/proc/cpuinfo", "r") as cpuinfo_handler:
    cpu_info_file = cpuinfo_handler.readlines()
    cpuinfo_handler.close()
    for line in cpu_info_file:
        line = line.strip()
        if re.match("processor", line):
            # match finds only lines starting with regex
            CPU_count = CPU_count+1
        if re.match("model name", line):
            CPU_model = line.split(":").pop().strip()
        if re.match("cache size", line):
            CPU_cache = int(line.split(":").pop().strip().split(" ")[0]) / block_size_div(args.block_size)
with open("/proc/sys/kernel/osrelease", "r") as osrelease_handler:
    osrelease_file = osrelease_handler.readlines()
    osrelease_handler.close()
    line = osrelease_file.pop().strip()
    if len(line) != 0:
        OS_release = line.strip()

with open("/proc/uptime", "r") as uptime_handler:
    uptime_file = uptime_handler.readlines()
    uptime_handler.close()
    uptime = float(str(uptime_file[0]).split(" ")[0].strip())
    uptime_h = math.floor(uptime / 3600)
    uptime_m = math.floor((uptime % 3600) / 60)
    uptime_s = uptime - (3600 * uptime_h) - (60 * uptime_m)
    uptime = f"{str(uptime_h)}h {str(uptime_m)}m {str('{0:.1f}'.format(uptime_s))}s"

with open("/proc/meminfo", "r") as meminfo_handler:
    meminfo_file = meminfo_handler.readlines()
    meminfo_handler.close()
    for line in meminfo_file:
        if re.match("MemTotal", line):
            memory = int(line.split(":").pop().strip().split(" ")[0]) / block_size_div(args.block_size)
            memory = str('{0:.1f}'.format(memory))

print("Processor:".ljust(20), CPU_model)
print("Number of cores:".ljust(20), CPU_count)
print("Cache memory:".ljust(20), CPU_cache, args.block_size.split().pop())
print("Memory (total):".ljust(20), memory, args.block_size.split().pop())
print("Kernel version:".ljust(20), OS_release)
print("Uptime:".ljust(20), uptime)
